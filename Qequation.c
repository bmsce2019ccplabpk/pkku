#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    float r1,r2,d;
    printf("enter the co efficients of the quadratic equation\n");
    scanf("%d%d%d",&a,&b,&c);
    d=(b*b)-4*(a*c);
    if(d>0)
    {
        printf("roots are real and distinct\n");
        r1=(-b+sqrt(d))/2*a;
        r2=(-b-sqrt(d))/2*a;
         printf("%f%f",r1,r2);
    }
    else if(d==0)
    {
        printf("roots are equal\n");
        r1=(-b)/2*a;
        r2=(-b)/2*a;
         printf("%f and %f are roots of the equation",r1,r2);
    }
    else
    {
        printf("roots are imaginary\n");
        printf("roots are (%f+i(%f))/%f\n",(-b),(sqrt(-d)),(2*a));
        printf("roots are (%f-i(%f))/%f",(-b),(sqrt(-d)),(2*a));
   
    }
    return 0;
}