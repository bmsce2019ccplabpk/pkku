
#include <stdio.h>
int main()
{
	FILE *fp;
    char c;
	fp=fopen("INPUT.txt","w"); //opening the file to write
    printf("Enter contents to be written in file\n");
	while((c=getchar())!=EOF)
	{
	   putc(c,fp);
	}
	fclose(fp);                //closing the file
	fp=fopen("INPUT.txt","r"); //opening the file to read
    printf("Contents are-->\n");
	while((c=getc(fp))!=EOF)
	{
		printf("%c",c);
	}
	fclose(fp);                //close file
    return 0;
}
